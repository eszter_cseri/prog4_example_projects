﻿using CarShop.Data;
using CarShop.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.GUI
{
    /// <summary>
    /// ViewModel for ModelListWindow
    /// </summary>
    internal class BaseModelListWindowViewModel
    {
        private IRepository<BaseModel> repository;

        /// <summary>
        /// All BaseModels.
        /// </summary>
        public IList<BaseModel> BaseModels
        {
            get { return repository.GetAll().ToList(); }
        }

        /// <summary>
        /// Creates the ViewModel.
        /// </summary>
        public BaseModelListWindowViewModel(IRepository<BaseModel> repository)
        {
            this.repository = repository;
        }
    }
}
