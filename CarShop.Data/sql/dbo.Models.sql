﻿CREATE TABLE [dbo].[Models]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [BaseModelId] INT NOT NULL, 
    CONSTRAINT [FK_Model_ToBaseModel] FOREIGN KEY ([BaseModelId]) REFERENCES [BaseModels]([Id])
)
